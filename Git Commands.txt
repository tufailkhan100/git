git --version

git --list

git config --global user.name ""

git config --global user.email <email>

clear

git init | Initializing the folder as Git Repository

git status | Checking the Git Repository status

git add . | Adding files to the staging area

git commit -m "Message"| Commiting to the Git Repository

git log --oneline | Checking for log of Git commands

git checkout commit_no file_name | Checking out a file from an earlier commit

git reset HEAD index.html | Resetting the Git Repository

git reset --hard HEAD^ | To remove the last commit from Git

git remote add origin <url> | set the local Git repository to set its remote origin

git push -u origin master | Pushing your commits to the online repository

git close <repository url> cloning an online repository